package controllers

type RetrieveController struct {
	BaseController
}

//定义一个找回用户密码结构
func (this *RetrieveController) OpenRetrievePage() {
	this.TplName = "retrieve.tpl"
}

//找回用户结构内容
func (this *RetrieveController) SaveRetrieve() {
	//页面取值
	phone := this.GetString("phone")
	password := this.GetString("password")
	rs, _ := dbConn.GetOne("select * from info_user where phone=?", phone)
	//如果输入的phone与查库里查询到的phone一致，则执行修改动作。如果输入的phone与查库里查询到的phone不一致，则执行返回并提示找回失败
	if (phone) == rs["phone"] {
		dbConn.GetOne("update info_user set password=? where phone = ?", password, phone)
		this.ApiSuccess("找回成功", "")
	} else {
		this.ApiError("找回失败，请确认用户名", "")
	}
}
