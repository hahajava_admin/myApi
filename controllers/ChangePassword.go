package controllers

type ChangePasswordController struct {
	BaseController
}

//定义一个修改用户密码结构
func (this *ChangePasswordController) OpenChangePasswordPage() {
	this.TplName = "changePwd.tpl"
}

//修改用户密码结构内容
func (this *ChangePasswordController) ChangePassword() {
	//1、页面取值
	phone := this.GetString("phone")
	password := this.GetString("password")
	rs, _ := dbConn.GetOne("select * from info_user where phone=?", phone)

	//2、将值更新到数据库
	if (phone) == rs["phone"] {
		if (password) != rs["password"] {
			dbConn.GetOne("update info_user set password=? where phone = ?", password, phone)
			this.ApiSuccess("修改成功", "")
		} else {
			this.ApiError("修改密码与原密码相同", "")
		}
	}else {
		this.ApiError("修改失败", "")
}
}