package controllers

import "myApi/models"

type UserController struct {
	BaseController
}

//定义一个注册用户结构
func (this *UserController) OpenRegisterPage() {
	this.TplName = "register.tpl"
}

//注册用户结构内容
func (this *UserController) SaveRegister() {
	phone := this.GetString("phone")
	password := this.GetString("password")
	name := this.GetString("name")
	id := models.GetSnowflakeId()
	//查询数据库判断库里是否存在输入phone值
	rs, _ := dbConn.GetOne("select * from info_user where phone=?", phone)
	//如果输入的phone与查库里查询到的phone不一致，则执行入库动作。如果输入的phone与查库里查询到的phone一致，则执行返回并提示用户名存在
	if (phone) != rs["phone"] {
		dbConn.GetOne("insert into info_user(id,phone,password,name) values(?,?,?,?)", id, phone, password, name)
		this.ApiSuccess("注册成功", "")
	} else {
		this.ApiError("注册失败，用户名已存在", "")
	}
}
