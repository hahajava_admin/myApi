package controllers

import (
	"github.com/astaxie/beego"
	"myApi/models"
)

var dbConn  = &models.DBConn{}

type BaseController struct {
	beego.Controller
}


//JSON输出
func (c *BaseController) ApiJson(code int16,msg string,data interface{}){
	if data == nil{
		data = ""
	}
	c.Data["json"] = map[string]interface{}{"code": code,"msg": msg,"data": data}
	c.ServeJSON()
	c.StopRun()
}
//返回成功的API成功
func (c *BaseController) ApiSuccess(msg string,data interface{} ){
	c.ApiJson(200,msg,data)
}
//返回失败的API请求
func (c *BaseController) ApiError(msg string,data interface{})  {
	c.ApiJson(-1,msg,data)
}
//返回失败且带code的API请求
func (c *BaseController) ApiErrorCode(msg string,data interface{},code int16)  {
	c.ApiJson(code,msg,data)
}