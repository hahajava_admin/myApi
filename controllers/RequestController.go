package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/astaxie/beego/httplib"
)

type RequestController struct {
	BaseController
}

type Header struct {
	Name   string `name`
	Value   string `value`
}

type Request struct {
	Name   string `name`
	Value   string `value`
	IsCheck   bool `isCheck`
}


func (this *RequestController) ReqGet()  {
	apiUrl := this.GetString("apiUrl")
	headerParams := this.GetString("headerParams")
	requestParams := this.GetString("requestParams")
	var headers []Header
	var requests []Request
	json.Unmarshal([]byte(headerParams),&headers)
	json.Unmarshal([]byte(requestParams),&requests)

	req := httplib.Get(apiUrl)
	for h := range headers{
		if len(headers[h].Name) >0{
			req.Header(headers[h].Name,headers[h].Value)
		}
	}
	for r := range requests{
		if len(requests[r].Name) >0{
			if requests[r].IsCheck  {
				fmt.Println(requests[r].Name)
				req.Param(requests[r].Name,requests[r].Value)
			}
		}
	}

	str,_:= req.String()
	fmt.Println(str)
	this.ApiSuccess("成功",str)
}

func (this *RequestController) ReqPost()  {
	apiUrl := this.GetString("apiUrl")
	req := httplib.Post(apiUrl)
	str,_:= req.String()
	fmt.Println(str)
	this.ApiSuccess("成功",str)
}