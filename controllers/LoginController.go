package controllers

import (
	"crypto/md5"
	"encoding/hex"
)

type LoginController struct {
	BaseController
}

func (c *LoginController) Home() {
	c.TplName = "home.tpl"
}

func (c *LoginController) LoginPage() {
	c.TplName = "login.tpl"
}

//登录
func (this *LoginController) Login()  {
	phone := this.GetString("phone")
	password := this.GetString("password")
	rs,_ :=dbConn.GetOne("select * from info_user where phone=?",phone)
	if len(rs)>0 {
		if MD5(password) != rs["password"] {
			this.ApiError("密码错误","")
		}
		rs["password"] = "******"
		this.SetSession("sessionUser",rs)
		this.SetSession("sessionUserId",rs["id"])
		this.ApiSuccess("成功",rs)
	}else{
		this.ApiError("用户名不存在","")
	}
}


// 生成32位MD5
func MD5(text string) string {
	ctx := md5.New()
	ctx.Write([]byte(text))
	return hex.EncodeToString(ctx.Sum(nil))
}