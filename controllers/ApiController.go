package controllers

import (
	"fmt"
	"myApi/models"
)

type ApiController struct {
	BaseController
}

//保存
func (this *ApiController) SaveApi()  {
	apiName := this.GetString("apiName")
	method := this.GetString("method")
	apiUrl := this.GetString("apiUrl")
	headerParams := this.GetString("headerParams")
	requestParams := this.GetString("requestParams")
	responseJson := this.GetString("responseJson")
	responseParams := this.GetString("responseParams")
	mockData := this.GetString("mockData")
	classifyId := this.GetString("classifyId")
	projectId := this.GetString("projectId")

	id:=models.GetSnowflakeId()
	sql := fmt.Sprintf("insert into info_api(id,classify_id,project_id,api_name,method,api_url,header_params,request_params,response_json,response_params,mock_data) value('%v','%v','%v','%s','%s','%s','%s','%s','%s','%s','%s') ",
		id,classifyId,projectId,apiName,method,apiUrl, headerParams,requestParams,responseJson,responseParams,mockData)
	rs,err := dbConn.GetOne(sql)
	fmt.Println(err)
	this.ApiSuccess("成功",rs)
}

//打开修改界面
func (this *ApiController) OpenModifyApiPage()  {
	this.TplName = "modifyApi.tpl"
}

//
func (this *ApiController) GetApiById()  {
	id := this.GetString("id")
	rs,err := dbConn.GetAll("select * from info_api where id=?",id)
	fmt.Println(err)
	this.ApiSuccess("成功",rs)
}

//修改保存
func (this *ApiController) ModifyApi()  {
	apiName := this.GetString("apiName")
	method := this.GetString("method")
	apiUrl := this.GetString("apiUrl")
	headerParams := this.GetString("headerParams")
	requestParams := this.GetString("requestParams")
	responseJson := this.GetString("responseJson")
	responseParams := this.GetString("responseParams")
	mockData := this.GetString("mockData")
	classifyId := this.GetString("classifyId")
	id := this.GetString("id")
	sql := fmt.Sprintf("update info_api set classify_id='%v',api_name='%v',method='%v',api_url='%v',header_params='%v',request_params='%v',response_json='%v',response_params='%v',mock_data='%v' where id='%v'",
		classifyId,apiName,method,apiUrl, headerParams,requestParams,responseJson,responseParams,mockData,id)
	fmt.Println(sql)
	dbConn.GetOne(sql)
	this.ApiSuccess("成功","")
}
