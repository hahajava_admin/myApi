package controllers

import (
	"fmt"
	"myApi/models"
)

type SystemController struct {
	BaseController
}

//查询左侧菜单
func (this *SystemController) QueryLeftMenuList()  {
	projectId := this.GetString("projectId")
	rs,_ := dbConn.GetAll("select id,classify_name name, pid pId from info_classify a where exists(select 1 from info_api where classify_id=a.id and project_id= ?)" +
		" union all select id,api_name name,classify_id pId from info_api where project_id= ? ",projectId,projectId)
	this.ApiSuccess("成功",rs)
}

//添加分类
func (this *SystemController) AddClassify() {
	classifyName := this.GetString("classifyName")
	proId := this.GetString("proId")
	id:=models.GetSnowflakeId()
	sql := fmt.Sprintf("insert into info_classify(id,pro_id,classify_name) value('%v','%v','%s') ",id,proId,classifyName)
	dbConn.GetOne(sql)
	this.ApiSuccess("成功","rs")
}

//查询分类
func (this *SystemController) GetClassifyByProId()  {
	proId := this.GetString("proId")
	sql := fmt.Sprintf("select * from info_classify where pro_id='%v'",proId)
	rs,_ := dbConn.GetAll(sql)
	this.ApiSuccess("成功",rs)
}