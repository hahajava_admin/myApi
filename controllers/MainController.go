package controllers

import (
	"fmt"
	"github.com/astaxie/beego"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Index() {
	c.Data["User"] = c.GetSession("sessionUser")
	userId :=c.GetSession("sessionUserId")
	rs,_ := dbConn.GetAll("SELECT * from info_project where user_id=? and `status`=0 ORDER BY create_time desc",userId)
	c.Data["ProList"] = rs
	c.Data["ProInfo"] = rs[0]
	c.TplName = "index.tpl"
}

func (this *MainController) Main() {
	this.Data["User"] = this.GetSession("sessionUser")
	userId :=this.GetSession("sessionUserId")
	rs,_ := dbConn.GetAll("SELECT * from info_project where user_id=? and `status`=0 ORDER BY create_time desc",userId)
	this.Data["ProList"] = rs
	fmt.Println(rs)
	this.TplName = "main.tpl"
}


func (c *MainController) Add() {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "addApi.tpl"
}
