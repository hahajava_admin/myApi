package routers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"myApi/controllers"
)

var FilterUser = func(ctx *context.Context) {
	userName := ctx.Input.Session("sessionUserId")
	if userName == nil {
		ctx.Redirect(302, "/")
	}
}



func init() {
	// 验证用户是否已经登录
	beego.InsertFilter("/addApi",beego.BeforeRouter,FilterUser)
	beego.InsertFilter("/main",beego.BeforeRouter,FilterUser)
	beego.InsertFilter("/openApiPage",beego.BeforeRouter,FilterUser)
	beego.InsertFilter("/index",beego.BeforeRouter,FilterUser)
	beego.InsertFilter("/api/*",beego.BeforeRouter,FilterUser)
	beego.InsertFilter("/sys/*",beego.BeforeRouter,FilterUser)


    beego.Router("/", &controllers.LoginController{},"get:Home")
	beego.Router("/addApi", &controllers.MainController{}, "get:Add")
	beego.Router("/main", &controllers.MainController{}, "get:Main")
	beego.Router("/openApiPage", &controllers.ApiController{}, "get:OpenModifyApiPage")
	beego.Router("/index", &controllers.MainController{},"get:Index")
	beego.Router("/login", &controllers.LoginController{},"post:Login")
	beego.Router("/login", &controllers.LoginController{},"get:LoginPage")
	//注册
    beego.Router("/register", &controllers.UserController{},"get:OpenRegisterPage")
	beego.Router("/saveRegister", &controllers.UserController{},"post:SaveRegister")
    //找回密码
    beego.Router("/retrieve", &controllers.RetrieveController{},"get:OpenRetrievePage")
	beego.Router("/saveRetrieve", &controllers.RetrieveController{},"post:SaveRetrieve")
    //main页内修改密码
    beego.Router("/changePwd", &controllers.ChangePasswordController{},"get:OpenChangePasswordPage")
	beego.Router("/changePassword", &controllers.ChangePasswordController{},"post:ChangePassword")

	ns:=beego.NewNamespace("/api",
		beego.NSRouter("/saveApi", &controllers.ApiController{},"post:SaveApi"),
		beego.NSRouter("/ReqPost", &controllers.RequestController{},"post:ReqPost"),
		beego.NSRouter("/ReqGet", &controllers.RequestController{},"get:ReqGet"),
		beego.NSRouter("/GetApiById", &controllers.ApiController{},"get:GetApiById"),
		beego.NSRouter("/modifyApi", &controllers.ApiController{},"post:ModifyApi"),
	)
	beego.AddNamespace(ns)

	sys:=beego.NewNamespace("/sys",

		beego.NSRouter("/QueryLeftMenuList", &controllers.SystemController{},"post:QueryLeftMenuList"),
		beego.NSRouter("/AddClassify", &controllers.SystemController{},"post:AddClassify"),
		beego.NSRouter("/GetClassifyByProId", &controllers.SystemController{},"post:GetClassifyByProId"),
		beego.NSRouter("/addPro", &controllers.ProjectController{},"post:AddPro"),
	)
	beego.AddNamespace(sys)
}
