<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>myApi</title>
    <link rel="stylesheet" href="/static/home/css/index.css">
</head>
<body>
<div class="box">
    <!--header-->
    <header class="header">
        <section class="header__wrap">
            <div class="header__logo">myApi</div>
            <nav class="header__nav">
                <a href="javascript:;" class="header__nav__item header__nav__item__status__active">首&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;页</a>
                <a href="javascript:;" class="header__nav__item">插&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;件</a>
                <a href="javascript:;" class="header__nav__item">教&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;程</a>
                <a href="/login" class="header__nav__item header__nav__item--study-now" id="study">登录</a>
                <section class="header_nav_tip"></section>
            </nav>
        </section>
    </header>
    <!--screen1-->
    <section class="screen-1" id="study1">
        <section class="screen-1__practice-courses" ></section>
        <section class="container">
            <h2 class="screen-1__heading screen-1__heading_animate_init">
                专属云上接口协作管理工具
            </h2>
            <p class="screen-1__heading-descrip screen-1__heading-descrip_animate_init">
                协作之上，提升开发效率，规范开发流程
            </p>
        </section>

    </section>
    <!--screen2-->
    <section class="screen-2">
        <section class="container">
            <h1 class="screen-2__heading">我们精心为您打造</h1>
            <section class="screen-2__sub-heading-tip"></section>
            <p class="screen-2__sub-heading">http / websocket本地，在线接口，都可以调。免费的团队协作工具，极致的文档编写体验，加快开发效率<br/>
                大牛带你体验BAT真实开发流程，所有开发过程一一为你呈现</p>
            <section class="screen-2__banner">
                <section class="screen-2__banner_i_1"></section>
                <section class="screen-2__banner_i_2"></section>
                <!--screen-2__banner_i_2人物图-->
                <section class="screen-2__banner_i_3"></section>
                <!--screen-2__banner_i_3火箭图-->
            </section>
        </section>
    </section>
    <!--screen3-->
    <section class="screen-3">
        <section class="screen-3__icon"></section>
        <section class="screen-3__content container">
            <section class="screen-3__content__heading">
                <h1>强大的功能支持</h1>
            </section>
            <section class="screen-3__content__tip"></section>
            <section class="screen-3__content__subheading">
                <p>让前后端无缝配合，让你节约前后端沟通成本</p>
            </section>
            <section class="screen-3__content__items">
                <span class="screen-3__content__items-1">文档生成</span>
                <span class="screen-3__content__items-2">在线调试</span>
                <span class="screen-3__content__items-4">批量测试</span>
                <span class="screen-3__content__items-5">Mock</span>
            </section>
        </section>
    </section>
    <!--screen4-->
    <section class="screen-4">
        <section class="screen-4__heading"><h2>省去本地复杂的环境搭建</h2></section>
        <section class="screen-4__tips"></section>
        <section class="screen-4__subheading"><p>让接口调试就是这么简单</p></section>
        <section class="screen-4__items">
            <section class="screen-4__items-wrap1">
                <section class="screen-4__items-1-icon"></section>
                <section class="screen-4__items-title"><p>实战课程集成开发环境</p></section>
            </section>
            <section class="screen-4__items-wrap2">
                <section class="screen-4__items-2-icon"></section>
                <section class="screen-4__items-title"><p>内置终端命令行</p></section>
            </section>
            <section class="screen-4__items-wrap3">
                <section class="screen-4__items-3-icon"></section>
                <section class="screen-4__items-title"><p>编译你的应用程序</p></section>
            </section>
            <section class="screen-4__items-wrap4">
                <section class="screen-4__items-4-icon"></section>
                <section class="screen-4__items-title"><p>通过云端服务输出效果</p></section>
            </section>
        </section>
    </section>
    <!--screen5-->
    <section class="screen-5">
        <section class="screen-5_icon"></section>
        <section class="screen-5_heading">
            <h2>云端调试可以这样简单</h2>
        </section>
        <section class="screen-5_tip">

        </section>
        <section class="screen-5_subheading"><p>安装浏览器插件，调试云端也可以调试本地代码。就是这么随意</p></section>
    </section>
    <!--others-->
    <section class="screen-others">
        <a  class="screen-others_btn" href="#study1">继续了解体验</a>
    </section>
    <!--footer-->
    <section class="footer">
        <nav class="footer_link">
            <a href="#">网站首页</a>
            <a href="#">联系我们</a>
            <a href="#">关于我们</a>
            <a href="#">意见反馈</a>
            <a href="#">友情链接</a>
        </nav>
        <p>Copyright &copy; 2020 api996.cn All Rights Reserved |浙ICP备 20021128号</p>
    </section>
    <!--outline-->
    <section class="outline" style="display:none;">
        <a class="outline__nav" href="javascript:;">实战课程</a>
        <a class="outline__nav" href="javascript:;">商业案例</a>
        <a class="outline__nav" href="javascript:;">课程体现</a>
        <a class="outline__nav" href="javascript:;">集成环境</a>
        <a class="outline__nav" href="javascript:;">云端学习</a>
        <a class="outline__nav" href="#study">即刻学习</a>
    </section>
</div>
<script type="text/javascript" src="/static/home/js/index.js">
</script>
</body>
</html>