<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="renderer" content="webkit">
  <meta http-equiv="Cache-Control" content="no-siteapp" />
  <title>myApi</title>

  <meta name="keywords" content="myApi">
  <meta name="description" content="让前端更有效率的工作">

  <!--[if lt IE 9]>
  <meta http-equiv="refresh" content="0;ie.html" />
  <![endif]-->

  <link rel="shortcut icon" href="favicon.ico">
  <link href="/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
  <link href="/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
  <link href="/static/css/animate.min.css" rel="stylesheet">
  <link href="/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
  <link href="/static/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css" rel="stylesheet">
  <link href="/static/css/plugins/codemirror/codemirror.css" rel="stylesheet">
   <link href="/static/css/plugins/codemirror/ambiance.css" rel="stylesheet">
    <link href="/static/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="row" id="app">
  <div class="wrapper animated fadeInRight">
    <div class="row">
      <div class="col-sm-12">
        <div class="wrapper wrapper-content animated fadeInRight">
          <form id="myForm" method="post" class="form-horizontal">
            <div class="ibox-content p-xs">
              <div class="row">
                <div class="col-sm-12">
                  <div class="float-e-margins">
                    <div class="ibox-content p-xs" style="border: none;">
                          <div class="form-group m-n">
                            <label class="col-sm-1 control-label">接口名称</label>
                            <div class="col-sm-11">
                              <input id="apiName" name="apiName" type="text" class="form-control"/>
                            </div>
                          </div>
                          <div class="hr-line-dashed m-xs"></div>
                          <div class="form-group m-n">
                            <label class="col-sm-1 control-label">请求地址</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <select id="method" name="method" class="form-control" style="width: 80px">
                                            <option value="1">GET</option>
                                            <option value="2">POST</option>
                                            <option value="3">PUT</option>
                                            <option value="4">DELETE</option>
                                        </select>
                                    </div>
                                    <input id="apiUrl" name="apiUrl" type="text" class="form-control" placeholder="URL路径部分，示例：/Api/v1/info"/>
                                </div>
                            </div>
                              <div class="col-sm-1">
                                  <button type="button" class="btn btn-default test-send">测试</button>
                              </div>
                              <div class="col-sm-1">
                                  <button type="button" class="btn btn-warning" onclick="save()">保存</button>
                              </div>
                          </div>
                          <div class="hr-line-dashed m-xs"></div>
                          <div class="form-group m-n">
                            <label class="col-sm-1 control-label">分类</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <select   class="form-control" id="classifyId" name="classifyId">
                                        <option value="">请选择分类</option>
                                        <option v-for="li in classList" :value="li.id">[[li.classify_name]]</option>
                                    </select>
                                    <span class="input-group-btn">
                                        <button id="addClassify" type="button" class="btn btn-white btn-bitbucket">
                                            <i class="fa fa-plus"></i> 增加分类
                                        </button>
                                    </span>
                                </div>
                            </div>
                          </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="m-t-sm">
              <div class="tabs-container">
                <ul class="nav nav-tabs">
                  <li class=""><a data-toggle="tab" href="#tab-1" aria-expanded="false"> header参数</a>
                  </li>
                  <li class="active"><a data-toggle="tab" href="#tab-2" aria-expanded="true">请求参数</a>
                  </li>
                </ul>
                <div class="tab-content">
                  <div id="tab-1" class="tab-pane">
                    <div class="panel-body">
                      <div class="col-sm-12">
                        <table class="footable table table-stripped footable-loaded header-table">
                          <thead>
                              <th>名称</th>
                              <th>值</th>
                              <th>描述</th>
                              <th>操作</th>
                          </thead>
                          <tbody>
                              <tr class="gradeX footable-even" style="display: table-row;">
                                <td><input type="text" name="name" class="form-control input-sm fieldname" placeholder="Content-Type"> </td>
                                <td><input type="text" name="value" class="form-control input-sm value" placeholder="application/x-www-form-urlencoded"></td>
                                <td><input type="text" name="desc" class="form-control input-sm desc" placeholder="字段说明"></td>
                                <td class="center"><a><i class="fa fa-trash-o m-l-xs" style="font-size: 16px;color: #dddddd;"></i></a></td>
                              </tr>
                          </tbody>
                          <tfoot>
                              <tr>
                                <td>
                                  <a class="btn btn-default btn-sm btn-rounded addHeader" @click="addHeader" href="javascript:;"><i class="fa fa-plus"></i> Add Header</a>
                                </td>
                              </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>
                  <div id="tab-2" class="tab-pane active">
                    <div class="panel-body">
                      <div class="col-sm-12">
                        <table class="footable table table-stripped footable-loaded request-table">
                          <thead>
                            <th>字段名</th>
                            <th>类型</th>
                            <th>必填</th>
                            <th>参数</th>
                            <th>描述</th>
                            <th>操作</th>
                          </thead>
                          <tbody>
                              <tr class="gradeX footable-even" style="display: table-row;" data-id="1-0" data-padding-left="0"  data-subnodenum="0">
                                <td style="padding-left: 0px;">
                                  <div class="checkbox checkbox-success checkbox-circle" style="padding-top: 0px;">
                                    <input id="checkbox8" type="checkbox" checked="" class="checkbox">
                                    <label for="checkbox8">
                                      <input type="text" name="name" class="form-control input-sm fieldname" placeholder="字段名">
                                    </label>
                                  </div>
                                </td>
                                <td>
                                  <select name="valueType" class="form-control valueType">
                                    <option value="string" >string</option>
                                    <option value="number" >number</option>
                                    <option value="object" >object</option>
                                    <option value="array" >array</option>
                                    <option value="boolean" >boolean</option>
                                    <option value="null" >null</option>
                                  </select>
                                </td>
                                <td>
                                  <select name="valueMust" class="form-control valueMust">
                                    <option value="1" selected>是</option>
                                    <option value="2" >否</option>
                                  </select>
                                </td>
                                <td><input type="text" name="value" class="form-control input-sm value" placeholder="请求参数"> </td>
                                <td><input type="text" name="desc" class="form-control input-sm desc" placeholder="字段说明"> </td>
                                <td class="center"><a><i class="fa fa-trash-o m-l-xs" style="font-size: 16px;color: #dddddd;"></i></a></td>
                              </tr>
                          </tbody>
                          <tfoot>
                          <tr>
                            <td>
                              <a class="btn btn-default btn-sm btn-rounded addsibling" @click="addsibling" href="javascript:;">添加字段</a>
                            </td>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="m-t-sm">
              <div class="tabs-container">
                <ul class="nav nav-tabs">
                  <li class="active">
                      <a data-toggle="tab" href="#tab-5" aria-expanded="true">请求结果</a>
                  </li>
                  <li>
                      <a data-toggle="tab" href="#tab-3" aria-expanded="true">响应数据</a>
                  </li>
                    <li class="">
                        <a data-toggle="tab" href="#tab-6" aria-expanded="false">Mock</a>
                    </li>
                </ul>
                <div class="tab-content">
                  <div id="tab-5" class="tab-pane active">
                    <div class="panel-body">
                      <div class="col-sm-12">
                        <div class="m-t-sm">
                          <textarea id="resultInfo" class="form-control successgoback" rows="4" placeholder=""></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="tab-3" class="tab-pane">
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="m-t-sm">
                                <textarea id="responseJson" class="form-control" rows="4" placeholder=""></textarea>
                            </div>
                        </div>
                      <div class="col-sm-12">
                        <table class="footable table table-stripped footable-loaded response-table">
                          <thead>
                              <th>字段名</th>
                              <th>类型</th>
                              <th>描述</th>
                              <th>操作</th>
                          </thead>
                          <tbody>
                              <tr class="gradeX footable-even" style="display: table-row;">
                                  <td style="padding-left: 0px;">
                                    <input type="text" name="name" class="form-control input-sm fieldname" placeholder="字段名">
                                  </td>
                                  <td>
                                      <select name="valueType" class="form-control valueType">
                                        <option value="string" >string</option>
                                        <option value="number" >number</option>
                                        <option value="object" >object</option>
                                        <option value="array" >array</option>
                                        <option value="boolean" >boolean</option>
                                        <option value="null" >null</option>
                                      </select>
                                  </td>
                                  <td><input type="text" name="desc" class="form-control input-sm desc" placeholder="字段说明"> </td>
                                  <td class="center"><a><i class="fa fa-trash-o m-l-xs" style="color: #dddddd;font-size: 16px;"></i></a></td>
                              </tr>
                          </tbody>
                          <tfoot>
                          <tr>
                            <td>
                              <a class="btn btn-default btn-sm btn-rounded fieldDesc" @click="fieldDesc" href="javascript:;">添加字段</a>
                            </td>
                          </tr>
                          </tfoot>
                        </table>
                      </div>
                    </div>
                  </div>
                    <div id="tab-6" class="tab-pane">
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="m-t-sm">
                                    <textarea id="mockData"  rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</body>

<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/js/plugins/layer/layer.min.js"></script>
<script src="/static/js/hplus.min.js?v=4.1.0"></script>
<script type="text/javascript" src="/static/js/contabs.min.js"></script>
<script src="/static/js/plugins/pace/pace.min.js"></script>
<script src="/static/js/plugins/codemirror/codemirror.js"></script>
<script src="/static/js/plugins/codemirror/mode/javascript/javascript.js"></script>
<script src="/static/js/plugins/validate/validator.min.js"></script>
<script src="/static/js/ajaxDataController.js"></script>
<script src="/static/event/apiInfo.js"></script>
<script src="/static/event/common.js"></script>
<script src="/static/js/vue.min.js"></script>
<script src="/static/js/plugins/sweetalert/sweetalert.min.js"></script>
<script>
    var projectId  = top.common.request.getParam("projectId");
    var vue = new Vue({
        el:"#app",
        delimiters: ['[[', ']]'],
        data:{
            classList:[],

        },
        created: function () {
            this.GetClassifyByProId()
        },
        methods:{
            addHeader:function(){
                var obj = $(".addHeader");
                var field = addHeaderField();
                obj.parents("table").find("tbody").append(field)
            },
            addsibling:function(){
                var obj = $(".addsibling");
                newDataid = (new Date()).valueOf();
                var dataleft = 0;
                fieldStr = getFieldApi(newDataid, dataleft, 0, '');
                obj.parents("table").find("tbody").append(fieldStr)
            },
            fieldDesc:function(){
                var obj = $(".fieldDesc");
                newDataid = (new Date()).valueOf();
                var dataleft = 0;
                fieldStr = getFieldDescApi(newDataid, dataleft, 0, '');
                obj.parents(".footable").find("tbody").append(fieldStr)
            },
            GetClassifyByProId:function () {
                myAjax().post("/sys/GetClassifyByProId",{proId: projectId},function (data) {
                    vue.classList  = data.data
                });
            }
        }
    });

    //前台js表单验证
    var validator = new Validator('myForm',[
        {
            name:"apiName",
            display:"接口名称不能为空！",
            rules: 'required'
        },
        {
            name:"apiUrl",
            display:"请求地址不能为空！",
            rules: 'required'
        },
        {
            name:"classifyId",
            display:"分类不能为空！",
            rules: 'required'
        }
    ]);

$(function () {
    $('#addClassify').click(function () {

        swal({
            title:'分类设置',
            type:'input',
            showCancelButton:true,
            closeOnConfirm:false,
            confirmButtonText:'确定',
            cancelButtonText:'取消',
            animation:'slide-from-top',
            inputPlaceholder:'请输入分类名称'
        }, function (inputValue) {
            if(inputValue==false) return false;
            if(inputValue==''){
                return false;
            }
            myAjax().post("/sys/AddClassify",{proId: projectId,classifyName:inputValue},function (data) {
                vue.GetClassifyByProId()
                swal({
                    title: "保存成功",
                    type: "success"
                },function(isConfirm){
                    $("select[id='classifyId']>option").each(function(){
                        if($(this).text() == inputValue){
                            $(this).attr('selected', 'selected')
                        }
                    });
                });
            });
        });
    });


    //editor_one = CodeMirror.fromTextArea(document.getElementById("resultInfo"),{ mode:"application/json",lineNumbers:true,matchBrackets:true,styleActiveLine:true,theme:"ambiance"});
    editor_two = CodeMirror.fromTextArea(document.getElementById("mockData"),{lineNumbers:true,matchBrackets:true,styleActiveLine:true});

});
    var editor_one = null;
    var editor_two = null;

</script>
</html>
