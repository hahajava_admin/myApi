<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <title>myApi</title>

    <meta name="keywords" content="myApi">
    <meta name="description" content="让前端更有效率的工作">

    <!--[if lt IE 9]>
    <meta http-equiv="refresh" content="0;ie.html" />
    <![endif]-->
    <link href="/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="/static/css/animate.min.css" rel="stylesheet">
    <link href="/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
</head>

<body class="gray-bg top-navigation">
<div id="wrapper">
    <div id="page-wrapper" class="gray-bg ">
        <div class="row border-bottom white-bg">
            <nav class="navbar navbar-static-top" role="navigation">
                <div class="navbar-header">
                    <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                        <i class="fa fa-reorder"></i>
                    </button>
                    <a href="#" class="navbar-brand">myApi</a>
                </div>
                <div class="navbar-collapse collapse" id="navbar">

                    <ul class="nav navbar-top-links navbar-right">
                        <li>
                            <a href="login.html">
                                <i class="fa fa-sign-out"></i> 退出
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h5>项目列表</h5>
                            </div>
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-sm-1 navbar-right">
                                        <div class="input-group">
                                            <button type="button" class="btn btn-sm btn-primary" @click="addPro">
                                                <i class="fa fa-plus" aria-hidden="true"></i> 增加
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="table-responsive">
                                    <table class="table table-striped">
                                        <thead>
                                            <tr>
                                                <th>项目</th>
                                                <th>接口数量</th>
                                                <th>创建日期</th>
                                                <th>操作</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {{range $a,$b := .ProList}}
                                                <tr>
                                                    <td>{{$b.project_name}}</td>
                                                    <td>0</td>
                                                    <td>{{$b.create_time}}</td>
                                                    <td>
                                                        <a href="/index?projectId={{$b.id}}">
                                                            <i class="fa fa-cog text-navy"></i> 接口管理
                                                        </a>
                                                    </td>
                                                </tr>
                                            {{end}}
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/static/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/static/js/plugins/layer/layer.min.js"></script>
<script src="/static/js/hplus.min.js?v=4.1.0"></script>
<script type="text/javascript" src="/static/js/contabs.min.js"></script>
<script src="/static/js/plugins/pace/pace.min.js"></script>
<script src="/static/js/ajaxDataController.js"></script>
<script src="/static/event/common.js"></script>
<script src="/static/js/vue.min.js"></script>
<script src="/static/js/plugins/sweetalert/sweetalert.min.js"></script>
<script type="application/javascript">
    var vue = new Vue({
        el:"#wrapper",
        delimiters: ['[[', ']]'],
        data:{
            menuList:[]
        },
        created: function () {

        },
        methods:{
            addPro:function(){
                layer.prompt({title: '添加项目名称', formType: 0}, function(text, index){
                    layer.close(index);
                    myAjax().post("/sys/addPro",{projectName: text},function (data) {
                       location.reload();
                    });
                });
            }
        }
    });
</script>
</html>
