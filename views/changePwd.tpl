<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>myApi - 登录</title>
    <meta name="keywords" content="myApi 好用的接口管理工具">
    <meta name="description" content="GO语言开发，好用的接口管理工具">
    <link href="/static/css/bootstrap.min14ed.css?v=3.3.6" rel="stylesheet">
    <link href="/static/css/font-awesome.min93e3.css?v=4.4.0" rel="stylesheet">
    <link href="/static/css/animate.min.css" rel="stylesheet">
    <link href="/static/css/style.min862f.css?v=4.1.0" rel="stylesheet">
    <link href="/static/css/plugins/sweetalert/sweetalert.css" rel="stylesheet">
</head>

<body class="gray-bg">
<div class="middle-box text-center loginscreen  animated fadeInDown" id="app">
    <div>
        <div>
            <h2 style="color: #e6e6e6;">myApi</h2>
        </div>
        <h3>修改密码</h3>
        <form class="m-t" role="form" action="#">
            <div class="form-group">
                <input type="text" id="phone" class="form-control" placeholder="请输入当前用户名或手机号码" required="">
            </div>

            <div class="form-group">
                <input type="password" id="password" class="form-control" placeholder="密码" required="">
            </div>

            <div class="form-group">
                <input type="password" id="password" class="form-control" placeholder="确认密码" required="">
            </div>


            <button type="button" class="btn btn-primary block full-width m-b" @click="change">确认修改</button>
            <p class="text-muted text-center">

            </p>
        </form>
    </div>
</div>
</body>
<script src="/static/js/jquery.min.js?v=2.1.4"></script>
<script src="/static/js/bootstrap.min.js?v=3.3.6"></script>
<script src="/static/js/vue.min.js"></script>
<script src="/static/js/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/static/js/ajaxDataController.js"></script>
<script>
    var vue = new Vue({
        el: "#app",
        delimiters: ['[[', ']]'],
        data: {},
        created: function () {

        },
        methods: {
            change: function () {
                myAjax().post("/changePassword", {
                    phone: $("#phone").val(),
                    password: $("#password").val(),
                    name: $("#name").val()
                }, function (data) {
                    if (data.code == 200) {
                        location.href = "/main"
                    } else {
                        swal("提示！", data.msg, "warning");
                    }
                });
            }
        }
    });
</script>
</html>
