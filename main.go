package main

import (
	"github.com/astaxie/beego"
	_ "myApi/routers"
)

func main() {
	beego.Run(":9996")
}

