
//添加header
/*$(".footable").on("click", ".addHeader", function () {
    var obj = $(this);
    var field = addHeaderField();
    obj.parents("table").find("tbody").append(field)
});*/

//添加header
function addHeaderField(){
    field = '<tr class="gradeX footable-even" style="display: table-row;">\n' +
        '       <td><input type="text" name="name" class="form-control input-sm fieldname" placeholder="Content-Type"> </td>\n' +
        '       <td><input type="text" name="value" class="form-control input-sm value" placeholder="application/x-www-form-urlencoded"></td>\n' +
        '       <td><input type="text" name="desc" class="form-control input-sm desc" placeholder="字段说明"></td>\n' +
        '       <td class="center delete"><a onclick="del(this)"><i class="fa fa-trash-o m-l-xs" style="font-size: 16px;"></i></a></td>\n' +
        '    </tr>';
    return field
}


//添加字段，这是为了防止用户将所有字段删除
/*$(".footable").on("click", ".addsibling", function(){
    var obj = $(this);
    newDataid = (new Date()).valueOf();
    var dataleft = 0;
    fieldStr = getFieldApi(newDataid, dataleft, 0, '');
    obj.parents(".footable").find("tbody").append(fieldStr)

});*/

var JSON_VALUE_TYPES = ['string','object', 'array','number', 'boolean', 'null'];
//同级及子级字段添加
function getFieldApi(dataid, paddingleft, subnodenum, vType){

    var fieldApi = '<tr class="gradeX footable-even" style="display: table-row;">\n' +
        '              <td style="padding-left: 0px;">\n' +
        '                              <div class="checkbox checkbox-success checkbox-circle" style="padding-top: 0px;">\n' +
        '                                <input id="checkbox'+dataid+'" type="checkbox" checked="" class="checkbox">\n' +
        '                                <label for="checkbox'+dataid+'">\n' +
        '                                  <input type="text" name="name" class="form-control input-sm fieldname" placeholder="字段名">\n' +
        '                                </label>\n' +
        '                              </div>\n' +
        '              </td>' +
        '              <td>\n' +
        '                   <select name="valueType" class="form-control valueType">';
                                $.each(JSON_VALUE_TYPES, function(i, type){
                                    if(vType == type){
                                        fieldApi +='            <option value="'+type+'" selected>'+type+'</option>';
                                    }else{
                                        fieldApi +='            <option value="'+type+'" >'+type+'</option>';
                                    }
                                });
        fieldApi +='              </select>\n' +
        '              </td>\n' +
        '              <td>\n' +
        '                   <select name="valueMust" class="form-control valueMust">\n' +
        '                         <option value="1" >是</option>\n' +
        '                         <option value="2" >否</option>\n' +
        '                   </select>\n' +
        '              </td>\n' +
        '              <td><input type="text" name="value" class="form-control input-sm value" placeholder="请求参数"> </td>' +
        '              <td><input type="text" name="desc" class="form-control input-sm desc" placeholder="字段说明"></td>' +
        '              <td class="center delete"><a onclick="del(this)"><i class="fa fa-trash-o m-l-xs" style="font-size: 16px;"></i></a></td>' +
        '         </tr>';
    return fieldApi
}

//添加说明字段 fieldDesc
/*$(".footable").on("click", ".fieldDesc", function(){
    var obj = $(this);
    newDataid = (new Date()).valueOf();
    var dataleft = 0;
    fieldStr = getFieldDescApi(newDataid, dataleft, 0, '');
    obj.parents(".footable").find("tbody").append(fieldStr)

});*/
//同级及子级字段添加
function getFieldDescApi(dataid, paddingleft, subnodenum, vType){
    var fieldApi = '<tr class="gradeX footable-even" style="display: table-row;" data-id="'+dataid+'" data-padding-left="'+paddingleft+'"  data-subnodenum="'+subnodenum+'">\n' +
        '               <td style="padding-left: 0px;">\n' +
        '                    <input type="text" name="name" class="form-control input-sm fieldname" placeholder="字段名">\n' +
        '               </td>' +
        '              <td>\n' +
        '                   <select name="valueType" class="form-control valueType">';
                                $.each(JSON_VALUE_TYPES, function(i, type){
                                    if(vType == type){
                                        fieldApi +='            <option value="'+type+'" selected>'+type+'</option>';
                                    }else{
                                        fieldApi +='            <option value="'+type+'" >'+type+'</option>';
                                    }
                                });
    fieldApi +='              </select>\n' +
        '              </td>\n' +
        '              <td><input type="text" name="desc" class="form-control input-sm desc" placeholder="字段说明"></td>' +
        '              <td class="center delete"><a onclick="del(this)"><i class="fa fa-trash-o m-l-xs" style="font-size: 16px;"></i></a></td>' +
        '         </tr>';
    return fieldApi
}

function del(obj){
    var delObj = $(obj);
    delObj.parents("tr").remove();
}

/*//删除字段
$(".footable").on("click", ".delete", function(){
    var obj = $(this);
    obj.parents("tr").remove();
});*/



//获取header信息
function getHeaderInfo(){
    var gather = $(".header-table tbody tr");
    var content = [];
    gather.each(function(){
        var strobj = $(this);
        content.push({
            "name": strobj.find('.fieldname').val(),
            "value": strobj.find('.value').val(),
            "desc": strobj.find('.desc').val()
        });
    });
    return content
}

//获取请求信息
function getRequestInfo(){
    var gather = $(".request-table tbody tr");
    var content = [];
    gather.each(function(){
        var strobj = $(this);
        content.push({
            "name": strobj.find('.fieldname').val(),
            "valueType": strobj.find('.valueType').val(),
            "valueMust": strobj.find('.valueMust').val(),
            "desc": strobj.find('.desc').val(),
            "value": strobj.find('.value').val(),
            "isCheck":strobj.find('.checkbox').is(':checked')
        });
    });
    return content

}
//获取响应数据
function getResponseInfo(){
    var gather = $(".response-table tbody tr");
    var content = [];
    gather.each(function(){
        var strobj = $(this);
        content.push({
            "name": strobj.find('.fieldname').val(),
            "valueType": strobj.find('.valueType').val(),
            "desc": strobj.find('.desc').val()
        });
    });
    return content
}





function save() {
    if(validator.validate().errors.length==0){
        var projectId  = top.common.request.getParam("projectId");
        var params ={
            "projectId":projectId,
            "headerParams":         JSON.stringify(getHeaderInfo()),
            "requestParams":        JSON.stringify(getRequestInfo()),
            "responseParams":       JSON.stringify(getResponseInfo()),
            "classifyId":$("#classifyId").val(),
            "apiName":$("#apiName").val(),
            "method":$("#method").val(),
            "apiUrl":$("#apiUrl").val(),
            "responseJson":JSON.stringify($("#responseJson").val()),
            "mockData":JSON.stringify($("#mockData").val()),
        }
        myAjax().post("/api/saveApi",params,function (data) {
            swal("保存成功", "","success")
            top.vue.QueryLeftMenuList()
        });
    }
}

function modify() {
    if(validator.validate().errors.length==0){
        var params ={
            "headerParams":         JSON.stringify(getHeaderInfo()),
            "requestParams":        JSON.stringify(getRequestInfo()),
            "responseParams":       JSON.stringify(getResponseInfo()),
            "classifyId":$("#classifyId").val(),
            "apiName":$("#apiName").val(),
            "method":$("#method").val(),
            "apiUrl":$("#apiUrl").val(),
            "responseJson":JSON.stringify($("#responseJson").val()),
            "mockData":JSON.stringify($("#mockData").val()),
            "id":$("#id").val(),
        }

        myAjax().post("/api/modifyApi",params,function (data) {
            swal("修改成功", "","success");
            top.vue.QueryLeftMenuList()
        });
    }
}
